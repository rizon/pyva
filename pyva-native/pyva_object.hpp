
struct PyvaObject
{
	PyObject_HEAD
	PyObject *dict;
	jobject object;
	jobject parent; // if object is a Field or Method, this is the object that field or method is on
};

extern PyTypeObject PyvaObjectType, PyvaExceptionType;

inline bool IS_PYVA(PyObject *obj)
{
	return Py_TYPE(obj) == &PyvaObjectType || Py_TYPE(obj) == &PyvaExceptionType;
}


