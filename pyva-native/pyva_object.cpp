#include "python.hpp"
#include "pyva_object.hpp"
#include <stddef.h>

PyTypeObject PyvaObjectType = {
	PyObject_HEAD_INIT(nullptr)
};

PyTypeObject PyvaExceptionType = {
	PyObject_HEAD_INIT(nullptr)
};

static PySequenceMethods SequenceMethods;
static PyMappingMethods MappingMethods;

static int contains(PyvaObject *self, PyObject *what)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();
	Pyva *pyva = Pyva::Get();

	/* The cache should always have a strong reference to this Pyva object,
	 * because if the interpreter is invoking this function it must contain
	 * a reference to ths Pyva object, so the cache shouldn't have swapped it's
	 * reference to weak. *Our* reference (at self->object) is always weak, but
	 * we rely on the cache keeping a strong reference.
	 *
	 * This also asserts that this is a valid, tracked, Pyva object
	 */
	assert(pyva->IsStrong(self->object));

	if (self->dict != nullptr && PyDict_Contains(self->dict, what) > 0)
		return 1;

	const char *value = PyString_AsString(what);
	JNI::LocalReference<jstring> jname = env->NewStringUTF(value);

	Python::AllowThreads t;
	jobject obj = env->CallStaticObjectMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_getAttribute, static_cast<jobject>(pyva->pyva), self->object, static_cast<jstring>(jname));
	if (obj != nullptr)
		return 1;

	return 0;
}

static void pyva_destructor(PyvaObject *self)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();

	PyObject_GC_UnTrack(self);

	Py_XDECREF(self->dict);
	if (self->object != nullptr)
		env->DeleteWeakGlobalRef(self->object);
	if (self->parent != nullptr)
		env->DeleteWeakGlobalRef(self->parent);

	Py_TYPE(self)->tp_free(self);
}

static PyObject *get_attribute(PyvaObject *self, PyObject *name)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();
	Pyva *pyva = Pyva::Get();

	assert(pyva->IsStrong(self->object));

	const char *_name = PyString_AsString(name);
	JNI::LocalReference<jstring> jname = env->NewStringUTF(_name);

	jobject obj;
	{
		Python::AllowThreads t;
		obj = env->CallStaticObjectMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_getAttribute, static_cast<jobject>(pyva->pyva), self->object, static_cast<jstring>(jname));
	}
	if (obj == nullptr)
		return PyObject_GenericGetAttr(reinterpret_cast<PyObject *>(self), name);

	PyObject *py_object = to_python(obj);
	if (py_object == Py_None)
	{
		Py_DECREF(py_object);
		return PyObject_GenericGetAttr(reinterpret_cast<PyObject *>(self), name);
	}

	if (IS_PYVA(py_object))
	{
		PyvaObject *pyva_object = reinterpret_cast<PyvaObject *>(py_object);

		// Just created
		if (pyva_object->parent == nullptr)
			// Set owner of this attribute
			pyva_object->parent = env->NewWeakGlobalRef(self->object);
	}

	/* The reference from to_python() carries over to here, and we return it */

	return py_object;
}

static int set_attribute(PyvaObject *self, PyObject *name, PyObject *value)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();
	Pyva *pyva = Pyva::Get();

	assert(pyva->IsStrong(self->object));

	const char *_name = PyString_AsString(name);
	JNI::LocalReference<jstring> jname = env->NewStringUTF(_name);

	jobject obj = to_java(value);

	{
		Python::AllowThreads t;
		if (env->CallStaticBooleanMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_setAttribute, static_cast<jobject>(pyva->pyva), static_cast<jobject>(self->object), static_cast<jstring>(jname), obj))
			return 0;
	}

	return PyObject_GenericSetAttr(reinterpret_cast<PyObject *>(self), name, value);
}

static PyObject *to_str(PyvaObject *self)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();
	Pyva *pyva = Pyva::Get();

	assert(pyva->IsStrong(self->object));

	JNI::String str;
	{
		Python::AllowThreads t;
		str = static_cast<jstring>(env->CallObjectMethod(self->object, JNI::java_lang_Object_toString));
	}

	return PyString_FromString(str);
}

static long hash(PyvaObject *self)
{
	Python::AllowThreads t;
	return JNI::Hash(self->object);
}

static PyObject *call(PyvaObject *self, PyObject *args, PyObject *kw)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();
	Pyva *pyva = Pyva::Get();
	jobject obj;
	jobject jargs = to_java(args);

	assert(pyva->IsStrong(self->object));

	if (self->parent == nullptr)
	{
		/* If there is no parent reference, this might be a java.lang.Class, and we want to invoke a constructor */
		Python::AllowThreads t;
		obj = env->CallStaticObjectMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_invokeConstructor, static_cast<jobject>(pyva->pyva), self->object, jargs);
	}
	else
	{
		JNI::LocalReference<jobject> pref = env->NewLocalRef(self->parent);
		if (pref == nullptr)
		{
			/* This can happen if Python holds a reference to an (object, function) until the object
			 * is garbage collected. Not sure what else to do here?
			 */
			Py_RETURN_NONE;
		}

		Python::AllowThreads t;
		/* This assumes self->object is a method (the function net.rizon.pyva.Util.invoke accepts a method) */
		obj = env->CallStaticObjectMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_invoke, static_cast<jobject>(pyva->pyva), self->object, static_cast<jobject>(pref), jargs);
	}

	if (!env->ExceptionCheck())
	{
		return to_python(obj);
	}

	/* An exception was thrown */

	jthrowable ex = env->ExceptionOccurred();
	/* Get the root cause of the exception and then throw that. This is probably an invocation exception (because of the reflection) */
	ex = static_cast<jthrowable>(env->CallStaticObjectMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_getCause, ex));

	PyObject *type = reinterpret_cast<PyObject *>(&PyvaExceptionType);
	PyObject *pyex = to_python(ex); /* this object is of type PyvaExceptionType and is raisable within Python */

	/* PyErr_Restore takes ownership of references */
	Py_INCREF(type);
	PyErr_Restore(type, pyex, nullptr);

	/* now the traceback is empty, add Java traceback. */

	jobjectArray stackTraceInfo = static_cast<jobjectArray>(env->CallStaticObjectMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_splitException, ex));
	int arrayLen = env->GetArrayLength(stackTraceInfo);
	for (int i = 0; i < arrayLen; ++i)
	{
		jobjectArray frameInfo = static_cast<jobjectArray>(env->GetObjectArrayElement(stackTraceInfo, i));

		JNI::String className = static_cast<jstring>(env->GetObjectArrayElement(frameInfo, 0)),
			methodName = static_cast<jstring>(env->GetObjectArrayElement(frameInfo, 1)),
			fileName = static_cast<jstring>(env->GetObjectArrayElement(frameInfo, 2));
		int lineNumber = env->CallIntMethod(env->GetObjectArrayElement(frameInfo, 3), JNI::java_lang_Integer_intValue);

		/* -2 is magic and means this frame is native, which is probably us */
		if (lineNumber == -2)
			break;

		pyva->AddTracebackFrame(fileName, methodName, lineNumber);
	}

	/* Now when Python unwinds to find a handler for the exception, it will append its own frames as necessary */

	env->ExceptionClear();
	return nullptr;
}

static int traverse(PyvaObject *self, visitproc visit, void *arg)
{
	Py_VISIT(self->dict);

	return 0;
}

static int clear(PyvaObject *self)
{
	Py_CLEAR(self->dict);

	return 0;
}

static PyObject *iterator(PyvaObject *self)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();
	Pyva *pyva = Pyva::Get();

	assert(pyva->IsStrong(self->object));

	jobject obj;
	JNI::String name;
	{
		Python::AllowThreads t;
		obj = env->CallStaticObjectMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_getIterator, self->object);

		if (obj == nullptr)
			name = static_cast<jstring>(env->CallStaticObjectMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_getClassName, self->object));
	}

	if (obj == nullptr)
	{
		PyErr_Format(PyExc_TypeError, "'%.200s' object is not iterable", static_cast<const char *>(name));
		return nullptr;
	}

	return to_python(obj);
}

static PyObject *iterator_next(PyvaObject *self)
{
	JNI::Env jnienv;
	JNIEnv *env = jnienv.GetEnv();
	Pyva *pyva = Pyva::Get();

	assert(pyva->IsStrong(self->object));

	jobject obj;
	{
		Python::AllowThreads t;

		if (!env->CallStaticBooleanMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_hasNextIterator, self->object))
			return nullptr;

		obj = env->CallStaticObjectMethod(JNI::net_rizon_pyva_Util, JNI::net_rizon_pyva_Util_nextIterator, self->object);
	}

	return to_python(obj);
}

void Pyva::SetupPyvaObject(const char *name, PyTypeObject *obj)
{
	/* create my Pyva type */
	obj->tp_name = name;
	obj->tp_basicsize = sizeof(PyvaObject);
	obj->tp_dealloc = reinterpret_cast<destructor>(pyva_destructor);
	obj->tp_as_sequence = &SequenceMethods;
	obj->tp_as_mapping = &MappingMethods;
	obj->tp_hash = reinterpret_cast<hashfunc>(hash);
	obj->tp_call = reinterpret_cast<ternaryfunc>(call);
	obj->tp_str = reinterpret_cast<reprfunc>(to_str);
	obj->tp_getattro = reinterpret_cast<getattrofunc>(get_attribute);
	obj->tp_setattro = reinterpret_cast<setattrofunc>(set_attribute);
	obj->tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HAVE_GC | Py_TPFLAGS_HAVE_ITER;
	obj->tp_traverse = reinterpret_cast<traverseproc>(traverse);
	obj->tp_clear = reinterpret_cast<inquiry>(clear);
	obj->tp_iter = reinterpret_cast<getiterfunc>(iterator);
	obj->tp_iternext = reinterpret_cast<iternextfunc>(iterator_next);
	obj->tp_dictoffset = offsetof(PyvaObject, dict);

	SequenceMethods.sq_contains = reinterpret_cast<objobjproc>(contains);

	MappingMethods.mp_subscript = reinterpret_cast<binaryfunc>(get_attribute);
	MappingMethods.mp_ass_subscript = reinterpret_cast<objobjargproc>(set_attribute);

}

void Pyva::RegisterPyva()
{
	Python::Lock lock(this);

	SetupPyvaObject("Pyva", &PyvaObjectType);
	SetupPyvaObject("PyvaException", &PyvaExceptionType);

	PyvaExceptionType.tp_base = reinterpret_cast<_typeobject *>(PyExc_BaseException);

	PyType_Ready(&PyvaObjectType);
	PyType_Ready(&PyvaExceptionType);
}

