package net.rizon.pyva;

import java.io.File;

/* An instance of Pyva represents a Python interpreter */
public class Pyva
{
	static
	{
		System.load(new File(System.mapLibraryName("pyva")).getAbsolutePath());
	}

	public Pyva()
	{
		start();
	}

	private synchronized native void start();
	public synchronized native void stop();

	/* Init pyva.py for the importer. Should be called after addToSystemPath() if applicable */
	public void init() throws PyvaException
	{
		invoke("pyva", "init", new Importer());
	}

	public synchronized native void addToSystemPath(String path) throws PyvaException;
	public synchronized native int gc();

	public synchronized native Object invoke(String module_name, String function_name, Object... args) throws PyvaException;
}
